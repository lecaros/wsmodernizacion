/**
 * 
 */
package info.lecaros.api.config;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * @author lecaros
 *
 */
public class CustomDateSerializer extends JsonSerializer<Date> {
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

	@Override
	public void serialize(Date date, JsonGenerator jgen,
			SerializerProvider provider) throws IOException,
			JsonProcessingException {
		jgen.writeString(formatter.format(date));

	}
}
