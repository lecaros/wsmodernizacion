/**
 * 
 */
package info.lecaros.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.DBAddress;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

/**
 * @author lecaros
 *
 */
@Configuration
@EnableMongoRepositories(basePackages = "info.lecaros.api.data.repositories")
public class MongoConfiguration extends AbstractMongoConfiguration {

	private static final String DB_NAME = "curriculums";
	private static final int DB_PORT = 37498;
	private static final String DB_HOST = "ds037498.mongolab.com";
	private static final String DB_USERNAME = "demo";
	private static final String DB_PASSWORD = "demo";

	@Override
	protected String getDatabaseName() {
		return DB_NAME;
	}

	@Override
	@Bean
	public Mongo mongo() throws Exception {
		DBAddress addr = new DBAddress(DB_HOST, DB_PORT, DB_NAME);
		return MongoClient.connect(addr).getMongo();
	}

	@Override
	protected UserCredentials getUserCredentials() {
		return new UserCredentials(DB_USERNAME, DB_PASSWORD);
	}

}
