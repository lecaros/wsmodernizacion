/**
 * 
 */
package info.lecaros.api.data.repositories;

import info.lecaros.api.model.Curriculum;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author lecaros
 *
 */
public interface CurriculumRepository extends
		MongoRepository<Curriculum, String> {
	Curriculum findCurriculumByRut(int rut);

	List<Curriculum> findAll();

}
