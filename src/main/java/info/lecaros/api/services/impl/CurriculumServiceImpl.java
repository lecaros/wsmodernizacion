/**
 * 
 */
package info.lecaros.api.services.impl;

import info.lecaros.api.data.repositories.CurriculumRepository;
import info.lecaros.api.model.Curriculum;
import info.lecaros.api.services.CurriculumService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author lecaros
 *
 */
@Component
public class CurriculumServiceImpl implements CurriculumService {
	@Autowired
	private CurriculumRepository cvRepository;

	@Override
	public Curriculum findCurriculumByRut(int rut) {
		return cvRepository.findCurriculumByRut(rut);
	}

	@Override
	public List<Curriculum> findAll() {
		return cvRepository.findAll();
	}

	@Override
	public void save(Curriculum curriculum) {
		cvRepository.save(curriculum);
	}
}
