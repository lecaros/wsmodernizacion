package info.lecaros.api.services;

import info.lecaros.api.model.Curriculum;

import java.util.List;

/**
 * 
 */

/**
 * @author lecaros
 *
 */
public interface CurriculumService {
	/**
	 * Finds a single Curriculum based on rut.
	 * 
	 * @param rut
	 * @return
	 */
	Curriculum findCurriculumByRut(int rut);

	/**
	 * Returns all curriculums in database.
	 * <p>
	 * It should have pagination.
	 * 
	 * @return
	 */
	List<Curriculum> findAll();

	/**
	 * Saves/update given curriculum instance.
	 * 
	 * @param curriculum
	 */
	void save(Curriculum curriculum);

}
