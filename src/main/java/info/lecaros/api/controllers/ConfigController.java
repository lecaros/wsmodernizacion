/**
 * 
 */
package info.lecaros.api.controllers;

import info.lecaros.api.model.AcademicRecord;
import info.lecaros.api.model.Curriculum;
import info.lecaros.api.model.WorkExperience;
import info.lecaros.api.services.CurriculumService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Its solely resp is to load initial data.
 * 
 * @author lecaros
 *
 */
@Component
@RestController
@RequestMapping("/config")
public class ConfigController {
	@Autowired
	private CurriculumService cvService;

	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

	@RequestMapping("/loaddata")
	public String loadData() throws ParseException {
		int rut = 14189361;
		Curriculum cv = cvService.findCurriculumByRut(rut);
		if (cv == null) {
			cv = new Curriculum();
			cv.setRut(Long.valueOf(rut));
		}

		cv.setFullName("José Lecaros Cisterna");
		cv.setEmail("lecaros@lecaros.info");
		cv.setAddress("Teatinos 690");
		cv.setDv('0');
		cv.setPhoneNumber("97897800");

		WorkExperience work1 = new WorkExperience();
		work1.setDescription("Cajero de supermercado Baratus");
		work1.setFromDate(formatter.parse("2010/01/01"));
		work1.setToDate(formatter.parse("2010/03/31"));
		work1.setRole("Cajero");

		WorkExperience work2 = new WorkExperience();
		work2.setDescription("Acomodador de salas, proyectista, aseador y atención en barra de cine Oinks");
		work2.setFromDate(formatter.parse("2010/04/01"));
		work2.setToDate(formatter.parse("2010/06/30"));
		work2.setRole("Multihombre");

		cv.setPastJobs(Arrays.asList(work1, work2));

		AcademicRecord degree1 = new AcademicRecord();
		degree1.setDegree("Enseñanza media");
		degree1.setGraduationYear(2009);
		degree1.setSchool("Liceo F16");

		AcademicRecord degree2 = new AcademicRecord();
		degree2.setDegree("Programador");
		degree2.setGraduationYear(2011);
		degree2.setSchool("Universidad del Mar");

		cv.setAcademicFormation(Arrays.asList(degree1, degree2));

		HashMap<String, String> additionalInfo = new HashMap<String, String>();
		additionalInfo.put("SO", "Ubuntu/linux saucy");
		additionalInfo.put("AS", "embeded Tomcat");
		additionalInfo.put("IDE", "Eclipse for Java EE Developers");
		additionalInfo.put("Database", "MongoDb on mongolab.com");
		additionalInfo.put("Framework", "Spring 4 (spring boot)");

		cv.setOtherInfo(additionalInfo);

		cvService.save(cv);
		return "OK";

	}

}
