/**
 * Controller in charge of processing CV requests.
 */
package info.lecaros.api.controllers;

import info.lecaros.api.exceptions.CurriculumNotFoundException;
import info.lecaros.api.model.Curriculum;
import info.lecaros.api.services.CurriculumService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lecaros
 *
 */
@Component
@RestController
@RequestMapping("/curriculums")
public class CurriculumController {
	@Autowired
	private CurriculumService cvService;

	@RequestMapping(value = "/{rut}", method = RequestMethod.GET)
	public Curriculum getCvByRut(@PathVariable int rut) {
		Curriculum cv = cvService.findCurriculumByRut(rut);
		if (null == cv) {
			throw new CurriculumNotFoundException();
		}
		return cv;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Curriculum> findAll() {
		return cvService.findAll();
	}

	@ExceptionHandler(CurriculumNotFoundException.class)
	@ResponseStatus(org.springframework.http.HttpStatus.NOT_FOUND)
	public void badRequestHandler() {

	}
}
