/**
 * 
 */
package info.lecaros.api.model;

import info.lecaros.api.config.CustomDateSerializer;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author lecaros
 *
 */
public class WorkExperience {
	private Date fromDate;
	private Date toDate;
	private String role;
	private String description;

	/**
	 * Role or job name. (Developer, BA, DBA, etc.)
	 * 
	 * @return
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Role or job name. (Developer, BA, DBA, etc.)
	 * 
	 * @param role
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Describes work done while on this position.
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Describes work done while on this position.
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Start date of this job position.
	 * 
	 * @return
	 */
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * Start date of this job position.
	 * 
	 * @param fromDate
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * End date of this job position.
	 * 
	 * @return
	 */
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getToDate() {
		return toDate;
	}

	/**
	 * End date of this job position.
	 * 
	 * @param toDate
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
