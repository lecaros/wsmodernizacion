/**
 * 
 */
package info.lecaros.api.model;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;

/**
 * Contains required information to build a cv/resume
 * 
 * @author lecaros
 *
 */
public class Curriculum {

	@Id
	private String id;
	private Long rut;
	private char dv;
	private transient String rutWithDv;

	private String fullName;
	private String address;
	private String phoneNumber;
	private String email;

	private List<WorkExperience> pastJobs;
	private List<AcademicRecord> academicFormation;

	private Map<String, String> otherInfo;

	public Long getRut() {
		return rut;
	}

	public void setRut(Long rut) {
		this.rut = rut;
	}

	public char getDv() {
		return dv;
	}

	public void setDv(char dv) {
		this.dv = dv;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getRutWithDv() {
		return MessageFormat.format("{0}-{1}", rut, dv);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<WorkExperience> getPastJobs() {
		return pastJobs;
	}

	public void setPastJobs(List<WorkExperience> pastJobs) {
		this.pastJobs = pastJobs;
	}

	public List<AcademicRecord> getAcademicFormation() {
		return academicFormation;
	}

	public void setAcademicFormation(List<AcademicRecord> academicFormation) {
		this.academicFormation = academicFormation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, String> getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(Map<String, String> otherInfo) {
		this.otherInfo = otherInfo;
	}

}
