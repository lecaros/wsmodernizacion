/**
 * 
 */
package info.lecaros.api.model;

/**
 * @author lecaros
 *
 */
public class AcademicRecord {
	private String degree;
	private int graduationYear;
	private String school;

	/**
	 * Obtained degree.
	 * 
	 * @return
	 */
	public String getDegree() {
		return degree;
	}

	/**
	 * Obtained degree.
	 * 
	 * @param degree
	 */
	public void setDegree(String degree) {
		this.degree = degree;
	}

	/**
	 * Year the degree was finished.
	 * 
	 * @return
	 */
	public int getGraduationYear() {
		return graduationYear;
	}

	/**
	 * Year the degree was finished.
	 * 
	 * @param graduationYear
	 */
	public void setGraduationYear(int graduationYear) {
		this.graduationYear = graduationYear;
	}

	/**
	 * University or school.
	 * 
	 * @return
	 */
	public String getSchool() {
		return school;
	}

	/**
	 * University or school.
	 * 
	 * @param school
	 */
	public void setSchool(String school) {
		this.school = school;
	}

}
