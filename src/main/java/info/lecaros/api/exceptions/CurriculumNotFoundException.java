/**
 * 
 */
package info.lecaros.api.exceptions;

/**
 * @author lecaros
 *
 */
public class CurriculumNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5464878955419656335L;

}
